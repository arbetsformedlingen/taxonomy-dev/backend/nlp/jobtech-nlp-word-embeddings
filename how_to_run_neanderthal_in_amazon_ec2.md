# How to run Uncomplicate Neanderthal in Amazon EC2

### 1. Start a plain Ubuntu Server in EC2
Ubuntu Server 20.04 LTS (HVM), SSD Volume Type - ami-0767046d1677be5a0 (64-bit x86) / ami-08b6fc871ad49ff41 (64-bit Arm)

### 2. Choose an instance with a GPU like `g4dn.8xlarge`

### 3. Add hard disk space, i choose 60GB

### 4. Neanderthal is depending on JCuda so you need to install NVIDIA Cuda

Install the lateset version of Cuda. Now it is 11.3

https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_local

```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.3.1/local_installers/cuda-repo-ubuntu2004-11-3-local_11.3.1-465.19.01-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu2004-11-3-local_11.3.1-465.19.01-1_amd64.deb
sudo apt-key add /var/cuda-repo-ubuntu2004-11-3-local/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda
```

### 5. Install cuDNN 

You need to register at NVIDIA to be able to download cuDNN.
Register and download there two files.

```
https://developer.nvidia.com/compute/machine-learning/cudnn/secure/8.2.0.53/11.3_04222021/Ubuntu20_04-x64/libcudnn8_8.2.0.53-1+cuda11.3_amd64.deb

https://developer.nvidia.com/compute/machine-learning/cudnn/secure/8.2.0.53/11.3_04222021/Ubuntu20_04-x64/libcudnn8-dev_8.2.0.53-1+cuda11.3_amd64.deb
```

### 6. Copy the files to your AWS EC2 instance

```
scp -i ~/.ssh/YOUR_AMAZON_KEY.pem libcudnn8_8.2.0.53-1+cuda11.3_amd64.deb ssh ubuntu@YOUR_EC2_ADDRESS.amazonaws.com:~/

scp -i ~/.ssh/YOUR_AMAZON_KEY.pem libcudnn8-dev_8.2.0.53-1+cuda11.3_amd64.deb  ssh ubuntu@YOUR_EC2_ADDRESS.amazonaws.com:~/
```


### 7. Copy the files to your AWS EC2 instance

```
scp -i ~/.ssh/YOUR_AMAZON_KEY.pem libcudnn8_8.2.0.53-1+cuda11.3_amd64.deb ssh ubuntu@YOUR_EC2_ADDRESS.amazonaws.com:~/

scp -i ~/.ssh/YOUR_AMAZON_KEY.pem libcudnn8-dev_8.2.0.53-1+cuda11.3_amd64.deb  ssh ubuntu@YOUR_EC2_ADDRESS.amazonaws.com:~/
```


### 8. Install the cuDNN files

```
sudo dpkg -i libcudnn8_8.2.0.53-1+cuda11.3_amd64.deb
sudo dpkg -i libcudnn8-dev_8.2.0.53-1+cuda11.3_amd64.deb 
```

### 11. Install Clojure and Java and datomic client

Install Java

```
sudo apt-get install default-jdk
sudo apt-get update 
sudo apt-get install rlwrap

```
Install Clojure

https://clojure.org/guides/getting_started#_installation_on_linux

```

curl -O https://download.clojure.org/install/linux-install-1.10.3.849.sh
chmod +x linux-install-1.10.3.849.sh
sudo ./linux-install-1.10.3.849.sh

```

Install AWS client

```
sudo apt  install awscli
aws configure
```

Install datomic client

https://docs.datomic.com/cloud/releases.html#current

```
wget https://datomic-releases-1fc2183a.s3.amazonaws.com/tools/datomic-cli/datomic-cli-0.10.82.zip
unzip datomic-cli-0.10.82.zip
```

Start socksproxy with 

```
~/bin/datomic-cli/datomic client access DATOMIC_SYSTEM_NAME
```


### 12. Install Leningen

Create a directory named `bin` in you home directory and download the lein file there

```
mkdir bin

cd bin

wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein

```

Set it to be executable (chmod a+x ~/bin/lein)

```
chmod a+x ~/bin/lein
```

Place it on your $PATH where your shell can find it (eg. ~/bin)

```
export PATH=/home/ubuntu/bin:$PATH
```

Run it (lein) and it will download the self-install package


### 13. Clone Neanderthal

`git clone https://github.com/uncomplicate/neanderthal.git`

### 14. Update project.clj In `examples/hello-world` 

In examples/helloworld edit the `project.clj`

Under Java 9+, you have to explicitly enable base module.

Add this to :jvm-opts in your leiningen project (or the equivalent if you're using something else):
"--add-opens=java.base/jdk.internal.ref=ALL-UNNAMED"

 Remove the comment #_

 Also update the version of the neanderthal dependecy to use [uncomplicate/neanderthal "0.41.0"]


### 15. Run a repl

`lein deps`

`lein repl`

### 16. Try out the GPU with Neanderthal

```

(use '[uncomplicate.commons.core :refer [with-release]])  
(use '[uncomplicate.clojurecuda.core :refer [with-default]]) 
(use '[uncomplicate.neanderthal [core :refer [asum]] [cuda :refer [cuv with-default-engine]]])


(with-default
  (with-default-engine
    (with-release [gpu-x (cuv 1 -2 5)]
      (asum gpu-x))))
```

If you get an `UnsatisfiedLinkError` you need to let ubuntu know where your missing files are.

Start by trying to find the file on your computer with `find`

`sudo find / -name 'libmkl_rt.so'`

`/home/ubuntu/.javacpp/cache/mkl-2021.1-1.5.5-linux-x86_64-redist.jar/org/bytedeco/mkl/linux-x86_64/libmkl_rt.so`

I don't know if this is the correct way of doing things but the following instruction works.

In your `.bashrc` file add `/home/ubuntu/.javacpp/cache/mkl-2021.1-1.5.5-linux-x86_64-redist.jar/org/bytedeco/mkl/linux-x86_64` to your LD_PATH like this.

```export LD_LIBRARY_PATH=/home/ubuntu/.javacpp/cache/mkl-2021.1-1.5.5-linux-x86_64-redist.jar/org/bytedeco/mkl/linux-x86_64:$PATH```


You can also add it with `ldconfig` as described in this blog post/

See this tutorial for more information:  https://blog.andrewbeacock.com/2007/10/how-to-add-shared-libraries-to-linuxs.html


### 17. Install Emacs 27 text only

https://ubuntuhandbook.org/index.php/2020/09/install-emacs-27-1-ppa-ubuntu-20-04/

```
sudo apt update

sudo apt install emacs
```

Run emacs in daemon mode 

```
emacs --daemon
```

connect to emacs daemon
``` 
emacsclient -t
```


### 18. Install prelude 

https://github.com/bbatsov/prelude

```
curl -L https://git.io/epre | sh
```

Make a copy of prelude modules sample file

```
cp .emacs.d/sample/prelude-modules.el .emacs.d/personal/

```
Uncomment clojure etc.




