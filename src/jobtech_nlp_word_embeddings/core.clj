(ns jobtech-nlp-word-embeddings.core
  (:gen-class)
  (:require
   [clojure.math.combinatorics :as combo]
   [jobtech-nlp-word-embeddings.morphemes2vec :as m2v]

   [jobtech-nlp-word-embeddings.naive-math :as naive-math]
   [jobtech-nlp-word-embeddings.io :as io]
   [taoensso.nippy :as nippy]
   [neanderthal-stick.nippy-ext :refer [with-real-factory]]

   [clojure.java.io :as jio]

   [neanderthal-stick.core :as stick]
   [neanderthal-stick.experimental :as exp]

   [uncomplicate.neanderthal
    [core :refer :all]
    [native :refer :all]
    [linalg :as linalg]
    [auxil :as auxil]
    [vect-math :as vect-math]
    ]

   [uncomplicate.commons.core :refer [with-release
                                      let-release
                                      release
                                      double-fn]]
   [uncomplicate.fluokitten.core :refer [fmap!
                                         fmap
                                         foldmap
                                         fold]]


   [jobtech-nlp-word-embeddings.pca :as pca]

   ))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;; TODO return the morpheme corpus as well so you can save it to disk
(defn concepts->morpheme-vecs [concepts]
  (let [morpheme-corpus (m2v/build-morpheme-corpus (map :taxonomy/preferred-label concepts))
        word-to-morpheme-vec-fun (m2v/word->morpheme-vec-fun morpheme-corpus)
        morpheme-vecs (pmap (fn [concept]
                              (word-to-morpheme-vec-fun (:taxonomy/preferred-label concept))) concepts)]
    morpheme-vecs))

(defn concept-friends [concept concepts]
  (pmap #(assoc % :cosine-similarity   (m2v/cosine-similarity (:vec concept) (:vec %))) concepts))


(defn calc-cosine [a-vec]
  (fn [index v]
    {:index index
     :cosine-similarity (m2v/cosine-similarity a-vec v)}) )

(defn neighbour [a-vec other-vecs]
  (let [calc-cosine-fn (calc-cosine a-vec)]
    (map-indexed calc-cosine-fn other-vecs))
  )

;; TODO  Create a function that takes a concept-id and returns a list of related concepts
;; id->vec
;; vec->ids

(def model-filename "resources/model.nippy")
(def concepts-filename "resources/concepts.nippy")

(defn save-matrix-to-disk! [matrix filename]
  (exp/save-to-file! matrix filename))

(defn load-matrix-from-disk [filename]
  (exp/load-from-file! filename))

(defn save-data-to-disk! [data filename]
  (let [_ (dorun data)]
    (nippy/freeze-to-file (jio/file filename) data)))

(defn load-data-from-disk [filename]
  (nippy/thaw-from-file (jio/file filename)))

(defn save-concepts! [concepts]
  (save-data-to-disk! concepts concepts-filename))

(defn load-concepts []
  (load-data-from-disk concepts-filename))

(def load-concepts-memo (memoize load-concepts) )

(defn load-matrix []
  (load-matrix-from-disk model-filename)
  )

(def load-matrix-memo (memoize load-matrix))

;; (save-concepts! client/all-concepts)
;; (save-matrix-to-disk! (fge (concepts->morpheme-vecs  all-concepts)) model-filename )
;; (def all-vecs (load-matrix-from-disk model-filename))
;; (get-related-concepts (col all-vecs 10000))

(defn add-cosine-similarity [score all-concepts]
  (let [concept (nth all-concepts (:index score))]
    (assoc concept :cosine-similarity (:cosine-similarity score))
    )
  )


(defn get-related-concepts [v]
  (let [concept-matrix (load-matrix-from-disk model-filename)
        vecs (rows concept-matrix)
        vec-index-and-cosine (neighbour v vecs)
        related (reverse (sort-by :cosine-similarity vec-index-and-cosine))
        concepts (load-concepts)

        ]
    (map #(add-cosine-similarity % concepts) related )
    )
  )




(defn positions
  [pred coll]
  (keep-indexed (fn [idx x]
                  (when (pred x)
                    idx))
                coll))

(defn get-concept-index-by-id [id]
  (first (positions (fn [c] (= id (:taxonomy/id c))) (load-concepts-memo)))
  )

(defn get-related-concepts-by-id [id]
  (let [concepts (load-concepts)
        id-index (first (positions (fn [c]
                                     (= id (:taxonomy/id c))) concepts))

        concept-matrix (load-matrix-from-disk model-filename)
        concept-vector (row concept-matrix id-index)
        ]
    (get-related-concepts concept-vector)
    ))


(defn build-uber-model! []
  (let [g2v-model (io/load-matrix-from-disk "resources/data/graph2vec.nippy")
        m2v-model (io/load-matrix-from-disk "resources/data/morpheme2vec.nippy")
        uber-model (fge (doall (map concat g2v-model m2v-model)))
        ;; pca-model (pca/pca (fge (pca/transpose uber-model)) 100)
        _ (io/save-matrix-to-disk! uber-model (io/create-data-filename "uber-model") )
        ])
  )



;; TODO Slow and messy refactor...
(defn get-related-concepts-by-id-graph [id]
  (let [concepts (io/load-data-from-disk "resources/data/concept-ids.nippy"   )
        id-index (.indexOf concepts id)

        concept-matrix (load-matrix-from-disk "resources/data/uber-model.nippy")

        concept-vector (row concept-matrix id-index)

        vecs (rows concept-matrix)
        vec-index-and-cosine (neighbour concept-vector vecs)
        related (reverse (sort-by :cosine-similarity vec-index-and-cosine))
        concept-with-cosine-sim (map #(add-cosine-similarity %  (io/load-concepts-memo)) related )

        ]
    (map  (fn [index]
            (let [id  (nth concepts (:index index))]
              (filter (fn [c] (= id (:concept/id c) )  ) concept-with-cosine-sim )
              ) )  (take 10  related))
    )
  )


;;; works but the morpheme need to be scaled down
(defn get-related-concepts-by-id-model [id concept-matrix]
  (let [concepts (io/load-data-from-disk "resources/data/concept-ids.nippy"   )
        id-index (.indexOf concepts id)

        concept-vector (col concept-matrix id-index)

        vecs (cols concept-matrix)
        vec-index-and-cosine (neighbour concept-vector vecs)
        related (reverse (sort-by :cosine-similarity vec-index-and-cosine))
        concept-with-cosine-sim (map #(add-cosine-similarity %  (io/load-concepts-memo)) related )

        ]
    concept-with-cosine-sim
    )
  )

;; (fge (map concat (rows g2v-model) (rows m2v-model)))

;; (take 10  (filter #(= "occupation-name" (:concept/type %) (get-related-concepts-by-id-model "xLxq_sQ7_RzR" (io/load-matrix-from-disk "resources/data/uber-model-2.nippy"))))



(defn get-concept-vector [id]
  (row (load-matrix-memo) (get-concept-index-by-id id))
  )

(defn average-vectors [vs]
  (scal (/ 1 (count vs)) (apply xpy vs)   )
  )

;; TODO Filter out ids that has been sent in
;; TODO refactor functions to be more consinstent

(defn get-related-concepts-by-ids [ids limit]
  (take limit (if (< 1 (count ids))
                (let [concept-vectors (map get-concept-vector ids)
                      average-vector  (average-vectors concept-vectors)
                      ]
                  (get-related-concepts average-vector))
                (get-related-concepts-by-id (first ids))
                ))
  )




(comment

  ;; 1. Get concepts from API
  ;; 2. Save concept list to file (list order is important)
  ;; 3. Create vector representation of all concepts
  ;; 4. Use PCA to get a lower dimension like dim 100
  ;; 5. Save concept matrix to disk.
  ;; 6. Create function that takes a concept id looks up the corresponding vector and returns the closest concepts
  ;; 7. Make available via API
  ;; 8. Make a function that takes a list of concepts and returns closest vectors
  ;; 9. If the speed is too slow add hash algorithm

  )


(comment
  (take 4 client/all-concepts)
  (def lang-m (fge (pca/transpose (concepts->morpheme-vecs (take 4 client/all-concepts)))))
  (m2v/cosine-similarity (row lang-m 1) (row lang-m 2)) ;; 0.7071067932881648

  (def mini (pca/pca lang-m 2))
  (m2v/cosine-similarity (row mini 2) (row mini 1)) ;; 1.0000000260725574

  (defn positions
    [pred coll]
    (keep-indexed (fn [idx x]
                    (when (pred x)
                      idx))
                  coll))

  )

;; (m2v/cosine-similarity (row taxonomy 10000) (row taxonomy 10001))

(defn fetch-vecotorize-and-persist-concepts! []
  (let [concepts (io/load-concepts-memo)
        concept-matrix (fge (pca/transpose (concepts->morpheme-vecs concepts)))
        compressed-concept-matrix (pca/pca concept-matrix 300)
        _ (save-matrix-to-disk! compressed-concept-matrix model-filename)
        _ (save-concepts! concepts)
        ])
  )
