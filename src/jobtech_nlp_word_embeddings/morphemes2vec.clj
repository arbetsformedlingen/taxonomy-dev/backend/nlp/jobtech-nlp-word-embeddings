(ns jobtech-nlp-word-embeddings.morphemes2vec
  (:require [clojure.string :as s]
            [jobtech-nlp-stava.compound-splitter.stava :as stava]
            [jobtech-nlp-word-embeddings.io :as io]
            [jobtech-nlp-word-embeddings.naive-math :as naive-math]
            [uncomplicate.commons.core :refer [let-release with-release]]
            [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.linalg :as linalg]
            [uncomplicate.neanderthal.native :refer :all]))

(defn replace-noisy-characters [string]
  (s/trim (.replaceAll string  "[/,.\\[\\]\\(\\)]" " "))
  )

(def noisy-words #{"och" "s" ""})

(defn remove-noisy-words [strings]
  (remove #(contains? noisy-words %) strings)
  )

(defn split-word-into-morphemes [word]
  (let [cleaned-word (replace-noisy-characters (s/lower-case word))]
    (vec (set (conj (remove-noisy-words
                     (flatten (stava/split cleaned-word)))  cleaned-word)))
    )
  )

;; CORPUS


(def corpus ["javautvecklare"
             "barnpsykologi"
             "barnsjuksköterska"
             "barnskötare"
             "barnsköterska"
             "barnterapeut"
             "ambulanssjuksköterska"
             "ambulanssjukvårdare"
             "taxichaufför"
             "taxiförare"
             "taxiguide"
             "taxitelefonist"
             "Guldsmedsvaror/Smycken"
             "Grönytemaskiner, körvana"
             ])

(defn build-morpheme-corpus [corpus]
  (sort (set (flatten (map split-word-into-morphemes corpus)))))

(defn build-morpheme-lookup [morpheme-corpus]
  (into {}  (map-indexed (fn [i morph]  [morph i]) morpheme-corpus)))

(defn morphemes2vec [morphemes morpheme-corpus]
   (reduce (fn [acc element]
               (if (contains? (set morphemes) element)
                 (conj acc 1)
                 (conj acc 0))) [] morpheme-corpus))

(defn word->morpheme-vec-fun [morpheme-corpus]
  (fn [word]
    (morphemes2vec (split-word-into-morphemes word) morpheme-corpus)))


;; (save-concepts! client/all-concepts)
;; (save-matrix-to-disk! (fge (concepts->morpheme-vecs  all-concepts)) model-filename )
;; (def all-vecs (load-matrix-from-disk model-filename))
;; (get-related-concepts (col all-vecs 10000))


(defn build-morpheme-model []
  (let [
        preferred-labels (map :concept/preferred-label (io/load-concepts-memo))
        morpheme-corpus (build-morpheme-corpus preferred-labels)
        word-to-morpheme-vec-fun (word->morpheme-vec-fun morpheme-corpus)
        morpheme-vecs (pmap word-to-morpheme-vec-fun preferred-labels)]
    morpheme-vecs))


(comment

  (def amb-vec (morphemes2vec (split-word-into-morphemes   "ambulanssjukvårdare")))

  (def amb-skot-vec (morphemes2vec (split-word-into-morphemes  "ambulanssjuksköterska")))

  (naive-math/cosine-similarity amb-vec amb-skot-vec)
  0.5
  (def taxi-vec (morphemes2vec (split-word-into-morphemes  "taxitelefonist")))

  (naive-math/cosine-similarity amb-vec taxi-vec)
  0.0
  (def barn-sjuk-vec (morphemes2vec (split-word-into-morphemes  "barnsjuksköterska")))

  (naive-math/cosine-similarity amb-skot-vec barn-sjuk-vec)
  0.5
  (naive-math/cosine-similarity amb-skot-vec amb-vec)
  0.5
  (split-word-into-morphemes "ambulanssjuksköterska")

  ("ambulanssjuksköterska" "ambulans" "sjuk" "sköterska")
  (split-word-into-morphemes "ambulanssjukvårdare")

  ("ambulanssjukvårdare" "ambulans" "sjuk" "vårdare")

  (def barnsjukclown-vec (morphemes2vec (split-word-into-morphemes   "barnsjukclown")))

  (naive-math/cosine-similarity amb-skot-vec barnsjukclown-vec)
  0.35355339059327373)


(defn cosine-similarity
  "returns the cosine similarity between 2 vectors "
  [u v]
  (let [dot-product (dot u v)
        norm2u (nrm2 u)
        norm2v (nrm2 v)
        ]
    (if (some zero? [dot-product norm2u norm2v])
      0.0
      (/  dot-product norm2u norm2v)
      )
    )
  )
