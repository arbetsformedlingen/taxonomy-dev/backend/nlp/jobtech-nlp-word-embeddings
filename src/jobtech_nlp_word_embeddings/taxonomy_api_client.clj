(ns jobtech-nlp-word-embeddings.taxonomy-api-client
  (:require
   [cheshire.core :as ch]
   [clj-http.client :as client]
   )
  )

(def url (str "https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concepts"))

(def base-request {:as :json-strict
              :headers {"api-key" "111"
                        "Content-Type" "application/json"}
              ;;                :debug true
              })

(defn- retrieve-all-concepts [r]
  (:body (client/get url r)))

(defn- retrieve-all-concepts-sorted []
  (sort-by :taxonomy/id (retrieve-all-concepts-memo base-request)))

(def retrieve-all-concepts-memo (memoize retrieve-all-concepts))

(def all-concepts (retrieve-all-concepts-memo base-request))

(defn all-concepts-of-type [type]
  (retrieve-all-concepts-memo (assoc base-request :query-params {"type" type}))
  )

(def all-skills (all-concepts-of-type "skill"))
