(ns jobtech-nlp-word-embeddings.io
  (:gen-class)
  (:require [clojure.java.io :as jio]
            [neanderthal-stick.experimental :as exp]
            [taoensso.nippy :as nippy]))

(defn create-data-filename [name]
  (str "resources/data/" name ".nippy"))

(def model-filename (create-data-filename "model"))
(def concepts-filename (create-data-filename "concepts"))
(def concept-ids-filename (create-data-filename "concept-ids"))
(def concept-relations-filename (create-data-filename "concept-relations"))

(defn save-matrix-to-disk! [matrix filename]
  (exp/save-to-file! matrix filename))

(defn load-matrix-from-disk [filename]
  (exp/load-from-file! filename))

(defn save-data-to-disk! [data filename]
  (let [_ (dorun data)]
    (nippy/freeze-to-file (jio/file filename) data)))

(defn load-data-from-disk [filename]
  (nippy/thaw-from-file (jio/file filename)))

(defn save-concepts! [concepts]
  (save-data-to-disk! concepts concepts-filename))

(defn- load-concepts []
  (load-data-from-disk concepts-filename))

(def load-concepts-memo (memoize load-concepts))

(defn- load-concept-ids []
  (load-data-from-disk concept-ids-filename))

(def load-concept-ids-memo (memoize load-concept-ids))

(defn get-concept-index
  [id concept-ids]
  (let [index (.indexOf concept-ids id)]
    (if (= -1 index)
      (throw (Exception. (str "Index for concept with id '" id "' was not found!")))
      index
      )))

(defn- load-concept-relations []
  (load-data-from-disk concept-relations-filename))

(def load-concept-relations-memo (memoize load-concept-relations))

(defn load-matrix []
  (load-matrix-from-disk model-filename))

(def load-matrix-memo (memoize load-matrix))
