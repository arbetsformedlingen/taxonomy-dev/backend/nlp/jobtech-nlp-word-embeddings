(ns jobtech-nlp-word-embeddings.pca
  (:gen-class)
  (:require [uncomplicate.commons.core :refer [let-release]]
            [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.linalg :as linalg]))

(defn nth-column [matrix n]
  (for [row matrix] (nth row n)))

(defn transpose[matrix]
  (for [column (range (count (first matrix)))]
    (nth-column matrix column)))

;; TODO check that submatrix dimensions is not too large
(defn pca [a dimensions]
  (let-release [pca-1 (linalg/svd  (copy a) :u :vt true)
                z1 (mm (:u pca-1) (:sigma pca-1))
                ]
    (submatrix z1 0 0 (mrows z1) dimensions)
    )
  )

(comment

  ;; staende matris
  (def test-matrix (fge  [[1 1 1 1 1]
                          [0 0 0 0 0]
                          [1 1 1 1 0.9] ]))

  (m2v/cosine-similarity (col test-matrix 0) (col test-matrix 2) ) ;; 1
  (m2v/cosine-similarity (col test-matrix 0) (col test-matrix 1) ) ;; 0

  (def test-vecs (concepts->morpheme-vecs test-concepts))

  (def a1 (fge (transpose test-vecs)))

  (def pca-1 (linalg/svd  (copy a1) :u :vt))

  (def z1 (mm (:u pca-1) (:sigma pca-1)))

  (m2v/cosine-similarity (row z1 3) (row z1 4) )
  (m2v/cosine-similarity (row z1 0) (row z1 1) )

  (def mini (submatrix z1 0 0 10 5))
  (m2v/cosine-similarity (row mini 0) (row mini 1) )

  )
