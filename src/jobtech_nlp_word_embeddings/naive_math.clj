(ns jobtech-nlp-word-embeddings.naive-math)


(comment

  ;; public static double cosineSimilarity(double[] vectorA, double[] vectorB) {
  ;;          double dotProduct = 0.0;
  ;;          double normA = 0.0;
  ;;          double normB = 0.0;
  ;;                 for (int i = 0; i < vectorA.length; i++) {
  ;;                      dotProduct += vectorA[i] * vectorB[i];
  ;;                      normA += Math.pow(vectorA[i], 2);
  ;;                      normB += Math.pow(vectorB[i], 2);
  ;;                      }q
  ;;         return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
  ;;                                                                           }

  )



(defn dot-product [vector-a vector-b]
  (reduce + (pmap * vector-a vector-b))
  )

;; (dot-product [1 2 3] [4 -5 6])
;; 12
;; https://mathinsight.org/dot_product_examples

(defn normalize-vector [vector-a]
  (reduce + (pmap * vector-a vector-a))
  )

(defn cosine-similarity [vector-a vector-b]
  (/ (dot-product vector-a vector-b)
     (* (Math/sqrt (normalize-vector vector-a))
        (Math/sqrt (normalize-vector vector-b)))
     )
  )

;; (cosine-similarity [2, 0, 1, 1, 0, 2, 1, 1] [2, 1, 1, 0, 1, 1, 1, 1])
;; 0.8215838362577491
;; https://stackoverflow.com/questions/1746501/can-someone-give-an-example-of-cosine-similarity-in-a-very-simple-graphical-wa


(defn magnitude-vector [v]
  (Math/sqrt (reduce + ( map  #(* % %) v))))

;; https://www.mathsisfun.com/algebra/vectors.html
;; b = (6,8) ?
;; |b| = √( 62 + 82 ) = √( 36+64 ) = √100 = 10


(defn unit-vector [v]
  (let [magnitude (magnitude-vector v)]
    (map #(/ % magnitude) v)
    )
  )
;; http://www.algebralab.org/lessons/lesson.aspx?file=Trigonometry_TrigVectorUnits.xml
;; unit-vector =  vector / vector-magnitude




;; https://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169



(defn euclidean-distance [point-a point-b]
  (Math/sqrt (reduce + (map #(Math/pow (- %1 %2) 2) point-a point-b)))
  )

;; (euclidean-distance [0 3 4 5] [7 6 3 -1])
;; 9.746794344808963
;; https://people.revoledu.com/kardi/tutorial/Similarity/EuclideanDistance.html
