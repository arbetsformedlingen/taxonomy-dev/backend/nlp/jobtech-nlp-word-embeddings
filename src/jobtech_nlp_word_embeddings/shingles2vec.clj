(ns jobtech-nlp-word-embeddings.shingles2vec
  (:require [clojure.math.combinatorics :as combo]
            [clojure.string :as s]
            [jobtech-nlp-word-embeddings.io :as io]
            [jobtech-nlp-word-embeddings.naive-math :as naive-math]
            [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]))

;; 228 229 246

(def alphabet (pmap char (concat [228  229 246]  (range 97 123) (range 48 58)  [32])))

;; (make-array Integer/TYPE 3)

(defn create-n-gram-of-alphabet [n]
  (map #(apply str %) (combo/selections alphabet n))
  )

(def all-possible-three-grams (memoize #(create-n-gram-of-alphabet 3)) )
(def all-possible-two-grams (memoize #(create-n-gram-of-alphabet 2)))
(def all-possible-two-grams-length (count (all-possible-two-grams)))

(def all-possible-one-grams (memoize #(create-n-gram-of-alphabet 1)) )
(def all-possible-one-grams-length (count (all-possible-one-grams)))

;; (set! *warn-on-reflection* true)
;; (set! *unchecked-math* :warn-on-boxed)

;; (def zero-array (make-array Integer/TYPE  (count (create-n-gram-of-alphabet 2))))

(defn inc-vector-at-index! [vec all-shingles shingle]
  (let [index (.indexOf all-shingles shingle)
        val (aget vec index)
        _ (aset vec index (inc val)  )
        ]
    vec
    )
  )

(defn create-vector-from-shingles-n1 [word-shingles]
  (let [v (make-array Integer/TYPE all-possible-one-grams-length)]
    (doseq
        [shingle word-shingles]
      (inc-vector-at-index! v (all-possible-one-grams) shingle)
      )
    (vec v)
    )
  )


(defn create-vector-from-shingles-n2 [word-shingles]
  (let [v (make-array Integer/TYPE all-possible-two-grams-length)]
    (doseq
        [shingle word-shingles]
      (inc-vector-at-index! v (all-possible-two-grams) shingle)
      )
    (vec v)
    )
  )

;;{:shingels ["gla" "las" "ass"]
;;   :buffer
;;   }

(defn normalize-string [string]
  (str " "  (s/trim (s/.replaceAll (s/lower-case string) "[^a-z0-9]" " ")) " " )
  )


(defn shingle-word
  ([size word]
   (shingle-word size (normalize-string word) [])
   )
  ([size word acc]
   (if (or (empty? word) (> size (count word)))
     acc
     (recur size (rest word) (conj acc (apply str (take size word))) )
     )
   )
  )

(defn shingles2vec [shingles]
  (reduce (fn [acc element]
            (if (contains? (set shingles) element)
              (conj acc 1)
              (conj acc 0)
              )
            ) [] (all-possible-three-grams))
  )

(defn word-to-3-shingle-vec [word]
  (shingles2vec (shingle-word 3 word))
  )

(defn shingle-taxonomy-n3 []
  (pmap
   word-to-3-shingle-vec
   (pmap :concept/preferred-label (io/load-concepts-memo)))
  )

(defn word-to-2-shingle-vec [word]
  (create-vector-from-shingles-n2 (shingle-word 2 word))
  )

(defn shingle-taxonomy-n2 []
  (pmap
   word-to-2-shingle-vec
   (pmap :concept/preferred-label (io/load-concepts-memo)))
  )

(defn word-to-1-shingle-vec [word]
  (create-vector-from-shingles-n1 (shingle-word 1 word))
  )



;; TODO scale down common shingles
(defn build-shingle2vec-model! []
  (let [model (dge (shingle-taxonomy-n2))
        ;; pca-model (pca/pca model 100)
        ]
    (io/save-matrix-to-disk! model (io/create-data-filename "shingle2vec-model2"))
    )
  )

;; (time (count (word-to-3-shingle-vec "kalasgurka")) )

;; (map sum (rows (dge (map word-to-2-shingle-vec corpus))))


(comment
  ;; https://stackoverflow.com/questions/26766799/what-is-the-corresponding-lapack-function-behind-matlab-suma-2-in-which-a-is

  (def corpus ["javautvecklare" "clojureutvecklare" "haskellutvecklare" "javanesiska" "kontaktmannaskap"])
  (def A (dge 5 40  (map word-to-1-shingle-vec corpus)  {:layout :column}))
  (def y (dv (repeat 40 1)))

  (def result (mv A y))

  (def carts (dge 2 4 [[10 0 7 3] [0 9 3 0]]))
  (def product-prices (dv [1.3 2.0 1.9 1.8]))
  (def result-2 (mv carts product-prices))

  )



(comment
  (def laxpastej-vec (shingles2vec (shingle-word 3 "laxpastej")))
  (def fiskgurka-vec (shingles2vec (shingle-word 3 "fiskgurka")))
  (naive-math/cosine-similarity laxpastej-vec fiskgurka-vec)
  0.0
  (def fiskpastej-vec (shingles2vec (shingle-word 3 "fiskpastej")))

 (naive-math/cosine-similarity laxpastej-vec fiskpastej-vec)
  0.5345224838248487

 (naive-math/cosine-similarity fiskgurka-vec fiskpastej-vec)
  0.26726124191242434

  )


(comment

"testa 2 shignles,
anvende .indexOf  och mutera en array istället
inc 1 om samma shingle förekommer flera gånger
"
  "1. shingla alla ord"
  "2. Stoppa in dem i en matris"
  "3. Summera vertikalt för att få frekvens för shinglen"
  "4. Få ut en vektor med summan"
  "5. [1/frekvens] * hela matrisen"
  )

(comment
  "Smart autocompete"
  "1. Create shingle vecs for all words in taxonomy"
  "2. Concat shingle vecs to graph vec"
  "3. When autocompleting. Shingle input word."
  "4. lookup closest shingle+graph vector"
  "5. Get related concept to looked up concept of correct type"

  "Happy path
   Lookup zumbaledar
   Finds zumbaledare, filter related occupation-names -> dansintruktör"
  )
