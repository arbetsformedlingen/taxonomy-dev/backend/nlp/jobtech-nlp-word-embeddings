(ns jobtech-nlp-word-embeddings.graph2vec
  (:require [jobtech-nlp-word-embeddings.io :as io]
            [jobtech-nlp-word-embeddings.pca :as pca]
            [uncomplicate.clojurecuda.core :refer [with-default]]
            [uncomplicate.commons.core :refer [with-release]]
            [uncomplicate.neanderthal.cuda :refer [cuge with-default-engine]]
            [uncomplicate.neanderthal.native :refer :all]))


(def relation-types ["broader" "narrower" "related" "substitutes"])

(defn get-vector-from-relation-type [type]
  (let [index (.indexOf relation-types type)
        zeros (vec (repeat (count relation-types) 0.0))]
    (if (= index -1)
      zeros
      (assoc zeros index 1.0))))

(defn hop-cost [hop]
  (get {0 1
        1 0.8
        2 0.4
        3 0.2
        4 0.1
        5 0.05
        6 0.05
        7 0.05
        8 0.05
        9 0.05} hop 0))

(defn find-neighbors
  "Returns the sequence of neighbors for the given node"
  [v coll]
  (get coll v))

(defn calc-hop-cost [node hops concept-ids]
  (let [index (io/get-concept-index node concept-ids)
        cost (hop-cost hops)]
    {index cost}))

(defn add-neighbor-distance [current-node neighbors neighbor-distance]
  (reduce (fn [acc element]
            (assoc acc element (inc (get neighbor-distance current-node)))) neighbor-distance (remove (set (keys neighbor-distance))  neighbors)))

(defn zeros [length]
  (vec (repeat length 0.0)))

(defn index-costs->vec [index-costs vec-length]
  (reduce (fn [acc element]
            (assoc acc (first element) (second element)))
          (zeros vec-length)
          index-costs))


;; TODO Refactor and rename this function. This is the function that I got working
(defn chomp-graph [graph v]
  (let [neighbor-distance {v 0}
        neighbors-1 (find-neighbors v graph)
        new-distance-1 (add-neighbor-distance v neighbors-1 neighbor-distance)
        un-visited-1 (remove (set (keys neighbor-distance))  neighbors-1)

        result-2 (pmap (fn [e]
                         (let [neighbors-2 (find-neighbors e graph)
                               new-distance-2 (add-neighbor-distance
                                               e
                                               neighbors-2
                                               new-distance-1)
                               un-visited-2  (remove (set (keys new-distance-2))  (concat neighbors-1 neighbors-2))]
                           [new-distance-2
                            un-visited-2])) un-visited-1)

        new-distance-3 (into {} (mapcat first result-2))]

    new-distance-3))

(defn chomp-graph-2-index-cost [id concept-relations concept-ids]
  (into {} (pmap (fn [[index cost]] (calc-hop-cost index cost concept-ids))
                 (chomp-graph concept-relations id))))

(defn chomp-concept-id->vec [id concept-relations concept-ids]
  (index-costs->vec (chomp-graph-2-index-cost id concept-relations concept-ids) (count concept-ids)))

(defn build-graph [concept-relations concept-ids]
  (pmap #(chomp-concept-id->vec % concept-relations concept-ids) concept-ids))

(defn build-taxonomy-graph []
  (let [concept-relations (io/load-concept-relations-memo)
        concept-ids (io/load-concept-ids-memo)
        ]
    (build-graph concept-relations concept-ids)
    )
  )
