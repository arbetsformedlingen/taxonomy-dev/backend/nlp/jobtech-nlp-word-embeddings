(ns jobtech-nlp-word-embeddings.build
  (:gen-class)
  (:require [jobtech-nlp-word-embeddings.graph2vec :as g2v]
            [jobtech-nlp-word-embeddings.io :as io]
            [jobtech-nlp-word-embeddings.morphemes2vec :as m2v]
            [jobtech-nlp-word-embeddings.pca :as pca]
            [jobtech-nlp-word-embeddings.db :as db]
            [uncomplicate.neanderthal.core :refer [rows scal]]
            [uncomplicate.neanderthal.native :refer [fge]]))

(defn build-and-save-graph-model! []
  (io/save-matrix-to-disk!
   (pca/pca (fge (pca/transpose
                  (g2v/build-taxonomy-graph   ))) 100)
   (io/create-data-filename "graph2vec"))
  )

(defn build-and-save-morpheme-model! []
  (io/save-matrix-to-disk!
   (pca/pca (fge (pca/transpose
                  (m2v/build-morpheme-model))) 100)
   (io/create-data-filename "morpheme2vec"))
  )

(defn build-uber-model! []
  (let [g2v-model (io/load-matrix-from-disk "resources/data/graph2vec.nippy")
        m2v-model (scal 0.2 (io/load-matrix-from-disk "resources/data/morpheme2vec.nippy"))
        uber-model (fge (map concat (rows g2v-model) (rows m2v-model)))
       ;; pca-model (pca/pca (fge (pca/transpose uber-model)) 100)
        _ (io/save-matrix-to-disk! uber-model (io/create-data-filename "uber-model") )
        ]
    uber-model
    )
  )


;;;------- Command-line API -------

(def tasks [["concept-ids" db/save-concept-ids-to-disk!]
            ["concepts" db/save-concepts-to-disk!]
            ["relations" db/save-relations!]
            ["graph-model" build-and-save-graph-model!]
            ["morpheme-model" build-and-save-morpheme-model!]
            ["uber-model" build-uber-model!]])

(defn -main [& args]
  (let [task-subset (into []
                          (case args
                            ["all"] identity
                            (filter (comp (set args) first)))
                          tasks)]
    (doseq [[key f] task-subset]
      (do 
        (println "* Running" key "...")
        (f)
        (println "  Done"))))
  (System/exit 0))
