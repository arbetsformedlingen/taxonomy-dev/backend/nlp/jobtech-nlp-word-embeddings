(ns jobtech-nlp-word-embeddings.db
  (:require [datomic.client.api :as d]
            [jobtech-nlp-word-embeddings.io :as io]))

(def datomic-config
  {:server-type :ion
   :region "eu-central-1"
   :system "tax-prod-v4"
   :endpoint "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port 8182
   :timeout 6000000000})

(defn- get-client [] (d/client datomic-config))

(defn- get-conn []
  (d/connect
   (get-client)
   {:db-name "jobtech-taxonomy-prod-read-2021-03-05-14-47-16"}))

(defn- get-db [] (d/db (get-conn)))

(def ^:private all-concept-ids-q
  '[:find ?id
    :where
    [_ :concept/id ?id]])

(defn- fetch-all-concept-ids []
  (doall (sort (flatten (d/q all-concept-ids-q (get-db))))))

(defn save-concept-ids-to-disk! []
  (io/save-data-to-disk! (fetch-all-concept-ids) io/concept-ids-filename))

(def ^:private all-concepts-q
  '[:find (pull ?c [:concept/id
                    :concept/type
                    :concept/definition
                    :concept/preferred-label
                    :concept/deprecated
                    :concept/quality-level
                    :concept/sort-order
                    :concept/alternative-labels
                    :concept/hidden-labels])
    :where
    [?c :concept/id ?id]])

(defn- fetch-all-concepts []
  (doall (sort-by :concept/id (flatten (d/q all-concepts-q (get-db))))))

(defn save-concepts-to-disk! []
  (io/save-data-to-disk! (fetch-all-concepts) io/concepts-filename)
  )


(def all-relations-q '[:find ?id1 ?id2
                       :where
                       [?r :relation/concept-1 ?c1]
                       [?r :relation/concept-2 ?c2]
                       [?r :relation/type ?rt]
                       [?c1 :concept/id ?id1]
                       [?c2 :concept/id ?id2]])

(defn- fetch-all-concept-relations []
  (d/q all-relations-q (get-db)))

(defn- all-relations-in-both-directions-grouped-by-concept-id []
  (group-by first (concat (fetch-all-concept-relations) (map reverse (fetch-all-concept-relations)))))

(defn- flatten-group-to-set [grouped-things]
  (reduce-kv (fn [m k v] (assoc m k (set (map second v))))  {} grouped-things))

;; TOdO remove
(defn- all-relations-grouped-by-concept-id []
  (doall (group-by first (fetch-all-concept-relations))))

(defn save-relations! []
  (io/save-data-to-disk!
   (doall (flatten-group-to-set
           (all-relations-in-both-directions-grouped-by-concept-id)))
   io/concept-relations-filename))
