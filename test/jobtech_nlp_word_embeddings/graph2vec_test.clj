(ns jobtech-nlp-word-embeddings.graph2vec-test
  (:require
   [clojure.test :refer :all]
   [jobtech-nlp-word-embeddings.morphemes2vec :as m2v]
   [jobtech-nlp-word-embeddings.graph2vec :as g2v]
   [jobtech-nlp-word-embeddings.io :as io]

   [jobtech-nlp-word-embeddings.pca :as pca]

   [uncomplicate.clojurecuda.core :refer [with-default]]

   ))


(def all-concepts-in-the-graph ["wYQU_ekV_AuQ" "6TEr_CBC_bqh" "icae_Y69_H8E" "yszw_TRw_S9p"])

(def the-graph
  {"wYQU_ekV_AuQ"  #{"6TEr_CBC_bqh" "icae_Y69_H8E"}
   "icae_Y69_H8E"  #{"yszw_TRw_S9p"}})

(defn test-calculate-graph-distance []
  (is (= {"wYQU_ekV_AuQ" 0,
          "icae_Y69_H8E" 1,
          "6TEr_CBC_bqh" 1,
          "yszw_TRw_S9p" 2}
         (g2v/chomp-graph the-graph "wYQU_ekV_AuQ")
         ))
  )

(defn calculate-graph-to-vec []
  (is (=
       [[1 0.8 0.8 0.4]
        [0.0 0.0]
        [0.0 0.0 1 0.8]
        [0.0 0.0]]
       (g2v/build-graph the-graph all-concepts-in-the-graph  )))
  )

(deftest graph-to-vec
  (calculate-graph-to-vec)
  (calculate-graph-distance)
  )
