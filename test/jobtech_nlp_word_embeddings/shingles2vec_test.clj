(ns jobtech-nlp-word-embeddings.shingles2vec-test
  (:require [jobtech-nlp-word-embeddings.shingles2vec :as sut]
            [clojure.test :as t]))

(def corpus ["javautvecklare"
             "clojureutvecklare"
             "haskellutvecklare"
             "javanesiska"
             "kontaktmannaskap"])
