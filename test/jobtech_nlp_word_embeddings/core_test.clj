(ns jobtech-nlp-word-embeddings.core-test
  (:require [clojure.test :refer :all]
            [jobtech-nlp-word-embeddings.core :refer :all]
            [jobtech-nlp-word-embeddings.graph2vec :refer :all]
            ))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 1))))


(deftest create-concept-relations-vec-test []
  (let [ids [:a :b :c]
        relations-grouped-by-id (group-by first [[:a :b]   [:a :c] [:b :c]])]

    (is (= [0.0 1.0 1.0]
           (create-concept-relations-vec :a ids relations-grouped-by-id)
           )))
  )
