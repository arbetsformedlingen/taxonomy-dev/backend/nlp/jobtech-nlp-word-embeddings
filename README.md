# jobtech-nlp-word-embeddings

Build vector representations of the Taxonomy.

## Installation

If it complains about mkl you need to install it.
See neanderhal documentation for more information. Otherwise lein should handle it out of the box, especially on linux.

https://neanderthal.uncomplicate.org/articles/getting_started.html#the-native-library-used-by-neanderthals-native-engine-optional

### macOS

The Intel MKL library is not available in brew and has to be installed manually from
https://software.intel.com/content/www/us/en/develop/tools/oneapi/base-toolkit.html. Click "Get it now".

After installing it some symlinks probably have to be added, https://github.com/uncomplicate/neanderthal/issues/31
```
ln -s /opt/intel/oneapi/mkl/latest/lib/* /usr/local/lib
```

## Usage

Start by opening a Datomic tunnel to the production database and editing the `jobtech-nlp-word-embeddings.db/datomic-config` var.

To build the full model, call
```
make all
```

The [taxonomy-vec](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec) project uses files produced by this code. Assuming that repository has the same parent directory as this repo, you can call
```
make deploy-to-taxonomy-vec
```
to copy the produced files to that repository.

Use the ```get-related-concepts-by-id``` to test the model with concept ids.


    $ java -jar jobtech-nlp-word-embeddings-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
