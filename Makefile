taxonomy_vec_dst = ../taxonomy-vec/resources
outputdir = resources/data
concept_ids = $(outputdir)/concept-ids.nippy
concepts = $(outputdir)/concepts.nippy
ubermodel = $(outputdir)/uber-model.nippy
files = $(concept_ids) $(concepts) $(outputdir)/morpheme2vec.nippy $(outputdir)/concept-relations.nippy  $(outputdir)/graph2vec.nippy $(ubermodel)
model = $(outputdir)/model.nippy

$(files):
	lein run all

$(taxonomy_vec_dst):
	echo "Could not locate the taxonomy-vec resources directory."

all: $(files)

deploy-to-taxonomy-vec: $(taxonomy_vec_dst) $(files)
	cp $(concept_ids) $(taxonomy_vec_dst)/data/. && cp $(concepts) $(taxonomy_vec_dst)/data/. && cp $(ubermodel) $(taxonomy_vec_dst)/models/. && cd $(taxonomy_vec_dst)/models/ && mv uber-model.nippy model.nippy
