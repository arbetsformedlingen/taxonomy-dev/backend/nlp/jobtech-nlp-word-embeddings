(defproject jobtech-nlp-word-embeddings "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [jobtech-nlp-stava "0.1.0"]
                 [com.datomic/client-cloud "0.8.78"]
                 [org.clojure/math.combinatorics "0.1.6"]

                 [clj-http "3.10.2"]
                 [cheshire "5.10.0"]

                 [com.taoensso/nippy "3.1.1"]
                 [uncomplicate/neanderthal "0.40.0"]
                 [org.bytedeco/mkl-platform-redist "2020.3-1.5.4"]

                 [neanderthal-stick "0.4.0"]

                 [uncomplicate/fluokitten "0.9.1"]
                 [uncomplicate/commons "0.12.0"]
                 [com.datomic/client-cloud  "0.8.105"]
                 ;;                 [se.jobtechdev.nlp.models/taxonomy-vec "1.1-20210212.141313-1"]

                 [javax.xml.bind/jaxb-api "2.3.1"]

                 ]

  ;; Nvidia doesn't ship CUDA for macOS; you have to add this to your project
  :exclusions [[org.jcuda/jcuda-natives :classifier "apple-x86_64"]
               [org.jcuda/jcublas-natives :classifier "apple-x86_64"]]

  :jvm-opts ^:replace ["--add-opens=java.base/jdk.internal.ref=ALL-UNNAMED"]

  :plugins [[lein-cljfmt "0.6.6"]
            [lein-ancient "0.6.15"]
            [lein-nsorg "0.3.0"]
            [jonase/eastwood "0.3.6"]]

  :main ^:skip-aot jobtech-nlp-word-embeddings.build
  :target-path "target/%s"
  :profiles {:uberjar     {:aot :all}
             :dev         [:project/dev :profiles/dev]
             :project/dev {:jvm-opts ["-Dconf=dev-config.edn"
                                      ~(str "-Djava.library.path=" (System/getenv "HOME") "/.clj-nativedep/jobtech-nlp-stava/0.1.0/linux-amd64")
                                      ]


                           }

             :profiles/dev {:dependencies [[lambdaisland/kaocha "1.0.829"]]}
             }
  :aliases {"kaocha" ["run" "-m" "kaocha.runner"]}


  )
